#!/bin/sh
echo "If you get errors you may need newer versions of automake and autoconf"
echo "You need at least automake 1.5 and autoconf 2.50"
aclocal $ACLOCAL_FLAGS
autoheader
#automake --add-missing --copy
autoconf
echo "Now you are ready to run ./configure"
